@extends('layouts.front')
@section('content')
<div class="row mt-4 ml-4">
            <div class="col-12">
                <div class="card">
  <div class="card-header text-left">
    All Students
    <div class="card-tools  text-right">
    <a class="btn btn-primary" href="{{route('student.create')}}">Add New</a>
    </div>
  </div>
  <div class="card-body">
<table class="table" id="student_listing">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  @if(isset($students) && $students->count() > 0)
  @php
  $i= 1;
  @endphp
    @foreach($students as $student)
       <tr>
        <td>{{$i}}</td>
        <td>{{$student->name}}</td>
        <td>{{$student->email}}</td>
        <td>{{$student->phone}}</td>
        <td><a class="confirm_delete" href="{{route('student.destroy',['id'=> $student->id])}}">Delete</a></td>
       </tr> 
       @php
       $i++;
       @endphp
    @endforeach
  @endif
  </tbody>
</table>
  </div>
</div>
</div>
</div>


@endsection