@extends('layouts.front')
@section('content')
<div class="row mt-4 ml-4">
            <div class="col-12">
                <div class="card">
  <div class="card-header text-left">
  All Students
  <div class="card-tools  text-right">
    <a class="btn btn-primary" href="{{route('student.index')}}">All Students</a>
    </div>
  </div>
  <div class="card-body text-left">
{{ Form::open(array('url' => route('student.store'))) }}
  @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    {{Form::text('name',NULL,['class'=>'form-control '. $errors->first('name', 'is-invalid')])}}
    @error('name')
    <div class="text-danger">{{ $message }}</div>
    @enderror
    </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    {{Form::text('email',NULL,['class'=>'form-control '. $errors->first('email', 'is-invalid')])}}
    @error('email')
    <div class="text-danger">{{ $message }}</div>
    @enderror
    </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    {{Form::text('phone',NULL,['class'=>'form-control '. $errors->first('phone', 'is-invalid')])}}
    @error('phone')
    <div class="text-danger">{{ $message }}</div>
    @enderror
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
{{Form::close()}}
  </div>
                </div></div>
</div>


@endsection