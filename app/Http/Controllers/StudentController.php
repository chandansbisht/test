<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = User::all();
        return view('student.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:100|unique:users',
            'name' => 'required|regex:/^[\pL\s]+$/u|max:50',
            'phone' => 'required|digits:10|max:10',
        ],['name.regex'=> 'Name field can not have special charecters']);
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->password = Hash::make('password');
        if($user->save()){
            $response['success'] = true;
            $response['message'] = 'Student Added successfully'; 
        }else{
            $response['error'] = true;
            $response['message'] = 'Something went wrong, please try again later'; 
        }
        return redirect(route('student.index'))->with($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = User::findOrFail($id);
        if($student->delete()){
                $response['success'] = true;
                $response['message'] = 'Student Deleted successfully'; 
            }else{
                $response['error'] = true;
                $response['message'] = 'Something went wrong, please try again later'; 
            }
        return redirect(route('student.index'))->with($response); 
    }
}
